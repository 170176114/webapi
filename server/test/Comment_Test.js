var app = require('../routes/router.js'),
    chai = require('chai'),
    request = require('supertest');

    var commentData = {
        ProductID: '5ec46ac94948913224a732db',
        Comment: 'Test001',
        Username: 'Testing'
      }

      describe('POST /comment', function () {
        it ('POST comment',  (done) => {
            request(app).post('/comment').send({commentData})
                .set('Accept', 'application/json').expect('Content-Type', /json/).expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    
    
                })
                done();
        })
    })


    describe('GET /comment', function () {
        it ('GET comment',  (done) => {
            request(app).get('/comment/:ProductID').send('5ec46ac94948913224a732db')
                .set('Accept', 'application/json').expect('Content-Type', /json/).expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    
    
                })
                done();
        })
    })

    describe('PUT /comment', function () {
        it ('PUT comment',  (done) => {
            request(app).put('/edit_comment/:id').send({Comment: "YES TEST"})
                .set('Accept', 'application/json').expect('Content-Type', /json/).expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    
    
                })
                done();
        })
    })

    describe('Delete /comment', function () {
        it ('Delete comment',  (done) => {
            request(app).delete('/delete_comment/:id').send({_id: "5ec46ac94948913224a732db"})
                .set('Accept', 'application/json').expect('Content-Type', /json/).expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    
    
                })
                done();
        })
    })



