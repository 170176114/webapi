var app = require('../routes/router.js'),
    chai = require('chai'),
    request = require('supertest');

    var itemData = {
        _id: '5ec46ac94948913224a732db',
        Productdescription: 'Test_Productdescription',
        Price: '123',
        Location: 'Test Location',
        Telephone: '123'
      }



    describe('GET /item', function () {
        it ('GET item',  (done) => {
            request(app).get('/item').send('5ec46ac94948913224a732db')
                .set('Accept', 'application/json').expect('Content-Type', /json/).expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    
    
                })
                done();
        })
    })

    describe('GET /item/:item_id', function () {
        it ('GET item/:item_id',  (done) => {
            request(app).get('/item').send('5ec46ac94948913224a732db')
                .set('Accept', 'application/json').expect('Content-Type', /json/).expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    
    
                })
                done();
        })
    })

    describe('PUT /update_item', function () {
        it ('PUT /update_item',  (done) => {
            request(app).put('/update_item').send({item_id: "YES TEST"})
                .set('Accept', 'application/json').expect('Content-Type', /json/).expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    
    
                })
                done();
        })
    })

    describe('Delete /delete_item', function () {
        it ('Delete delete_item',  (done) => {
            request(app).delete('/delete_item').send({_id: "5ec46ac94948913224a732db"})
                .set('Accept', 'application/json').expect('Content-Type', /json/).expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    
    
                })
                done();
        })
    })



