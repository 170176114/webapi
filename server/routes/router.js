var express = require('express');
var router = express.Router();
var User = require('../models/user');
var Item = require('../models/item');
var Items = require('../models/item_model');
var Comment = require('../models/comment');
var Comments = require('../models/comment_model');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var multer = require('multer');
const path = require('path')
const storage = multer.diskStorage({
  destination: function(req, file, cb){
    cb(null, './uploads/');
  },
  filename: function(req, file, cb){
    cb(null, file.originalname);
  }
});

const methodOverride = require('method-override');
router.use(methodOverride('_method'));

var upload = multer({storage: storage})

//mongoose.connect('mongodb+srv://304cem:Blacklotus123@cluster0-gfbbm.azure.mongodb.net/WebAPI');
var username = ''
var authemail = ''
var authpassword = ''

router.use(bodyParser.urlencoded({ extended: true }));
// GET route for homepage
// router.get('/', function (req, res, next) {
//   return res.jsonp({"name":username});

// });


//POST route for updating data
router.post('/', function (req, res, next) {

  if (req.body.password !== req.body.passwordConf) {
    var err = new Error('Password does not match');
    err.status = 400;
    res.json({
      status: 'failure',
      error: {
        code : '400',
        text : 'Password does not match!'
      }
    });
    return next(err);
  }

  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {

    var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
      passwordConf: req.body.passwordConf
    }

    User.create(userData, function (error, user) {
      if (error) {
        return next(error);
      } else {
        req.session.userId = user._id;
        username = user.username;
        return res.redirect('/username');
      }
    });

  } else if (req.body.logemail && req.body.logpassword) {
    authemail = req.body.logemail
    authpassword = req.body.logpassword
    User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
      if (error || !user) {
        var err = new Error('Wrong email or password');
        err.status = 401;
        return res.json({
          status: 'failure',
          error: {
            code : '401',
            text : 'Wrong email or password'
          }
        });
      } else {
        req.session.userId = user._id;
        username = user.username;
        exports.authemail = authemail
        exports.authpassword = authpassword
         res.redirect('/username'); // go to the page
        //res.send(user.username);
      }
    });
  } else {
    var err = new Error('All fields are required');
    err.status = 402;
    return res.json({
      status: 'failure',
      error: {
        code : '402',
        text : 'All fields are required'
      }
    });
  }
})

router.get("/username", function (req, res, next) {
  var loginUser =  req.session.userId;
  var isLogined = !!loginUser;
  // res.jsonp({
  //   isLogined: isLogined,
  //   name: loginUser || ''
  // });
  User.findById(loginUser, function (err, user) {
    res.jsonp({username: user})
  } );

  


})





// GET route to redirect to '/profile' page after registering
router.get('/main', function (req, res, next) {
  User.findById(req.session.userId)
    .exec(function (error, user) {

        if (user === null) {
          var err = new Error('');
          err.status = 403;
          res.json({
            status: 'failure',
            error: {
              code : '403',
              text : 'Fail to find user'
            }
          });
        } else {
          username = user.username;
          res.jsonp({"name":username});
          
          // res.sendFile(path.join(__dirname , '../views/main.html'));
          
          // res.render(path.join(__dirname , '../views/main.html'), {name:username})


          //return res.send('<h2>Your name: </h2>' + user.username + '<h2>Your email: </h2>' + user.email + '<br><a type="button" href="/logout">Logout</a>')
        
      }
    });
});
// router.post('/create', Items.ItemPost)

router.post('/item', upload.single('ProductImage') ,function (req, res, next){
  var image = req.file.filename
  //console.log(req.file)
  var CreateData = {
    ProductImage: image,
    ProductName: req.body.ProductName,
    Productdescription: req.body.Productdescription,
    Location: req.body.Location,
    Price: req.body.Price,
    Telephone: req.body.Telephone,
    Username: req.body.Username
  }

  User.authenticate(authemail, authpassword, function (error, user) {
    if (error || !user) {
      var err = new Error('Wrong email or password');
      err.status = 401;
      return res.json({
        status: 'failure',
        error: {
          code : '401',
          text : 'Wrong email or password'
        }
      });
    } else {
      req.session.userId = user._id;
      username = user.username;
      //res.redirect('/username'); // go to the page
      //res.send(user.username);

      Item.create(CreateData, function (error, user) {
        if (error) {
          console.log(error)
        } else {
          console.log({
            status: 'success',
            CreateData
          })
        }
      });


    }
  });


  

})


// router.get('/item', function (req,res,next) {
//   Item.find({}, function (err, item) {
//     //console.log(item)
//     res.jsonp(item)
//   })
// })

router.get('/item', Items.ItemGet);
router.get('/item/:item_id', Items.ItemGetOne)

// router.get('/item/:item_id', function (req, res) {
//   Item.findOne({ _id: req.params.item_id }, function (err, item) {
//     res.jsonp(item)
//   })
// })

// router.get("/image", function (req,res) {
//   // res.send(req.params.image_name)
//   res.sendFile(path.join(__dirname, '../uploads/belt.jpg'));
// })

router.get("/image/:image_name", function (req,res) {
  // res.send(req.params.image_name)
  res.sendFile(path.join(__dirname, '../uploads/' + req.params.image_name));
})

router.put('/item', Items.ItemPut);

// router.put("/update_item", function (req, res) {

//   Item.findByIdAndUpdate(req.body.item_id, {
//     $set: {
//       Productdescription: req.body.Productdescription,
//       Price: req.body.Price,
//       Location: req.body.Location,
//       Telephone: req.body.Telephone
//     }
//   }, (err, result) => {
//     if (err) return console.log(err)
//     console.log(result)
//   })
// });

router.delete("/item", Items.ItemDelete)

router.post('/comment', Comments.CommentPost)

// router.post('/comment',function (req, res, next){
//   var commentData = {
//     ProductID: req.body.ProductID,
//     Comment: req.body.Comment,
//     Username: req.body.Username
//   }
  
//   Comment.create(commentData, function (error, user) {
//     if (error) {
//       console.log(error)
//     } else {

//     }
//   });
// })
router.get('/comment/:ProductID', Comments.CommentGet)

// router.get('/comment/:ProductID', function (req, res, next) {
//   Comment.find({ProductID: req.params.ProductID} , function (err, item) {
//     res.jsonp(item)
//   })
// })

router.put("/comment/:id", Comments.CommentPut);

// router.put("/edit_comment/:id", function (req, res) {

//   Comment.findByIdAndUpdate(req.body.selected_id, {
//     $set: {
//       Comment: req.body.change_comment
//     }
//   }, (err, result) => {
//     if (err) return console.log(err)
//     console.log(result)
//   })
// });
router.delete("/comment/:id", Comments.CommentDelete)


// router.delete("/delete_comment/:id", function (req, res) {
//   Comment.findByIdAndDelete(req.body._method, (err, result) => {
//     if (err) return res.send(err)
//     console.log(result)
//   })
// })




// GET for logout
router.get('/logout', function (req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        username = "";
        return res.jsonp({"name":username});
      }
    });
  }
});



module.exports = router;