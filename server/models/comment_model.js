var Comment = require('../models/comment');
var User = require('../models/user')
var routes = require('../routes/router');

exports.CommentPost = function (req, res, next){
    var commentData = {
      ProductID: req.body.ProductID,
      Comment: req.body.Comment,
      Username: req.body.Username
    }


    User.authenticate(routes.authemail, routes.authpassword, function (error, user) {
      if (error || !user) {
        var err = new Error('Wrong email or password');
        err.status = 401;
        return res.json({
          status: 'failure',
          error: {
            code : '401',
            text : 'Wrong email or password'
          }
        });
      } else {
        req.session.userId = user._id;
        username = user.username;
        //res.redirect('/username'); // go to the page
        //res.send(user.username);
        Comment.create(commentData, function (error, user) {
          if (error) {
            console.log(error)
          } else {
            res.jsonp({
              status: "success",
              user
          })
          }
        });
    
    
    
      }
    });
    



  }

exports.CommentGet = function (req, res, next) {

  User.authenticate(routes.authemail, routes.authpassword, function (error, user) {
    if (error || !user) {
      var err = new Error('Wrong email or password');
      err.status = 401;
      return res.json({
        status: 'failure',
        error: {
          code : '401',
          text : 'Wrong email or password'
        }
      });
    } else {
      req.session.userId = user._id;
      username = user.username;
      //res.redirect('/username'); // go to the page
      //res.send(user.username);
      Comment.find({ProductID: req.params.ProductID} , function (err, item) {
        res.jsonp({
          status: "success",
          item
      })
      })
  
  
    }
  });





  }

exports.CommentPut = function (req, res) {


  User.authenticate(routes.authemail, routes.authpassword, function (error, user) {
    if (error || !user) {
      var err = new Error('Wrong email or password');
      err.status = 401;
      return res.json({
        status: 'failure',
        error: {
          code : '401',
          text : 'Wrong email or password'
        }
      });
    } else {
      req.session.userId = user._id;
      username = user.username;
      //res.redirect('/username'); // go to the page
      //res.send(user.username);
      Comment.findByIdAndUpdate(req.body.selected_id, {
        $set: {
          Comment: req.body.change_comment
        }
      }, (err, result) => {
        if (err) return console.log(err)
        res.jsonp({
          status: "success",
          result
      })
      })
  
  
    }
  });


  }


exports.CommentDelete = function (req, res) {


  User.authenticate(routes.authemail, routes.authpassword, function (error, user) {
    if (error || !user) {
      var err = new Error('Wrong email or password');
      err.status = 401;
      return res.json({
        status: 'failure',
        error: {
          code : '401',
          text : 'Wrong email or password'
        }
      });
    } else {
      req.session.userId = user._id;
      username = user.username;
      //res.redirect('/username'); // go to the page
      //res.send(user.username);
      Comment.findByIdAndDelete(req.body._method, (err, result) => {
        if (err) return res.send(err)
        res.jsonp({
          status: "success",
          result
      })
      })
  
  
    }
  });




  }