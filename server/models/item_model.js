var item = require('../models/item');
var routes = require('../routes/router');
var User = require('../models/user');
var multer = require('multer');
const path = require('path')
const storage = multer.diskStorage({
    destination: function(req, file, cb){
      cb(null, './uploads/');
    },
    filename: function(req, file, cb){
      cb(null, file.originalname);
    }
  });
var upload = multer({storage: storage})




exports.ItemGet = function (req, res, next){
  User.authenticate(routes.authemail, routes.authpassword, function (error, user) {
    if (error || !user) {
      var err = new Error('Wrong email or password');
      err.status = 401;
      return res.json({
        status: 'failure',
        error: {
          code : '401',
          text : 'Wrong email or password'
        }
      });
    } else {
      req.session.userId = user._id;
      username = user.username;
      //res.redirect('/username'); // go to the page
      //res.send(user.username);

      item.find({}, function (err, item) {
        //console.log(item)
        res.jsonp({
          status: "success",
          item
      })
      })


    }
  });
  

 



}

exports.ItemPost = function (req, res, next){
  var image = req.file.filename
  //console.log(req.file)
  var CreateData = {
    ProductImage: image,
    ProductName: req.body.ProductName,
    Productdescription: req.body.Productdescription,
    Location: req.body.Location,
    Price: req.body.Price,
    Telephone: req.body.Telephone,
    Username: req.body.Username
  }
  
  Item.create(CreateData, function (error, user) {
    if (error) {
      console.log(error)
    } else {

    }
  });
}

exports.ItemGetOne = function (req, res ,next){

  User.authenticate(routes.authemail, routes.authpassword, function (error, user) {
    if (error || !user) {
      var err = new Error('Wrong email or password');
      err.status = 401;
      return res.json({
        status: 'failure',
        error: {
          code : '401',
          text : 'Wrong email or password'
        }
      });
    } else {
      req.session.userId = user._id;
      username = user.username;
      //res.redirect('/username'); // go to the page
      //res.send(user.username);
      item.findOne({ _id: req.params.item_id }, function (err, item) {
        res.jsonp({
          status: "success",
          item
      })
        })
  
  
    }
  });







}

exports.ItemPut = function (req, res, next) {

  User.authenticate(routes.authemail, routes.authpassword, function (error, user) {
    if (error || !user) {
      var err = new Error('Wrong email or password');
      err.status = 401;
      return res.json({
        status: 'failure',
        error: {
          code : '401',
          text : 'Wrong email or password'
        }
      });
    } else {
      req.session.userId = user._id;
      username = user.username;
      //res.redirect('/username'); // go to the page
      //res.send(user.username);
      item.findByIdAndUpdate(req.body.item_id, {
        $set: {
          Productdescription: req.body.Productdescription,
          Price: req.body.Price,
          Location: req.body.Location,
          Telephone: req.body.Telephone
        }
      }, (err, result) => {
        if (err) return console.log(err)
        res.jsonp({
          status: "success",
          result
      })
      })

  
  
    }
  });




}

exports.ItemDelete = function (req, res, next) {

  User.authenticate(routes.authemail, routes.authpassword, function (error, user) {
    if (error || !user) {
      var err = new Error('Wrong email or password');
      err.status = 401;
      return res.json({
        status: 'failure',
        error: {
          code : '401',
          text : 'Wrong email or password'
        }
      });
    } else {
      req.session.userId = user._id;
      username = user.username;
      //res.redirect('/username'); // go to the page
      //res.send(user.username);
      item.findByIdAndDelete(req.body.item_id, (err, result) => {
        if (err) return res.send(err)
        res.jsonp({
          status: "success",
          result
      })
      })

  
  
    }
  });
  




}
